from urllib import response
from django.http import JsonResponse
from django.views import View
from api_calizo import api_ss
from .models import Model_Forms
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import requests
import json 

class RequesForms(View):
    def get(self, request):
        Clist= Model_Forms.objects.all()
        return JsonResponse(list(Clist.values()), safe=False)

class GetData(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs) :
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):

        data=json.loads(request.body) 
        response = api_ss.SendDataSharpSpring().SendData(request.body)

        return JsonResponse(response)





        



