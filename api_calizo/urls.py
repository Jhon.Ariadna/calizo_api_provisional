from django.urls import path
from .views import GetData, RequesForms

urlpatterns = [
    path('form_1/', RequesForms.as_view(), name='formulario 1'),
    path('data/', GetData.as_view(), name='get data')
]
